#!/usr/bin/perl
require "./functions.pl";


#HASH Para Centros
#HASH Para Trimestres

#HASH SANCIONADOS Key: ID Centro, Value: Hash de Trimestres = "Key: Trimestre, Value: Total"
sub sancionado{
	&clearScreen;
	print "Presupuesto Sancionado\n";
	print "\n";
	print "Ingrese el anio presupuestario: ";

	$paramAnio = &readSTDIN;

	%centros = &getCentros;
	%totalesTrimestre = &getSancionados;

	print "\n";
	print "1) Opcion CT (Ordenado por Codigo y Trimestre\n";
	print "2) Opcion TC (Ordenado por Trimestre y por Codigo\n";
	print "Seleccione que tipo de listado desea: ";


	$tipoSancionado = &readSTDIN;

	%hashTrimestres = &getTrimestres;
	$outputFile = "Sancionado";
	&openSalida($outputFile, $paramAnio);
	print SALIDA "Centro ID;Nombre de Centro;Trimestre;Total\n";

	if($tipoSancionado == 1)
	{

		#CT
		$totalAnual = 0;
		foreach my $centro (sort keys %totalesTrimestre) {

			$totalCentro = 0;
			print "Centro ID | Nombre de Centro | Trimestre | Total \n";
			print "---------------------------------------------------------\n";
			foreach my $trimestre (sort keys %{$totalesTrimestre{$centro}})
			{
				&customPrint("$centro | $centros{$centro} | $hashTrimestres{$trimestre} Trimestre $paramAnio | $totalesTrimestre{$centro}{$trimestre}");
				print "\n---------------------------------------------------------\n";
				$totalCentro+=$totalesTrimestre{$centro}{$trimestre};
				$totalAnual+=$totalesTrimestre{$centro}{$trimestre};
			}
			&customPrint("$centro | $centros{$centro} | TOTAL UNIDAD      | $totalCentro");
			print "\n---------------------------------------------------------\n";
			print "\n";
		}
		&customPrint("TOTAL ANUAL  $paramAnio     | $totalAnual \n");
		close (SALIDA);
		&imprimirReporte($outputFile, scalar(keys %totalesTrimestre));
	}
	elsif($tipoSancionado == 2)
	{
		#TC
		$totalAnual = 0;
		for ($i=0;$i<4;$i++)
		{

			$totalTrimestre = 0;
			print "Centro ID | Nombre de Centro | Trimestre | Total \n";
			print "---------------------------------------------------------\n";
			foreach my $centro (sort keys %totalesTrimestre)
			{
				&customPrint("$centro | $centros{$centro} | $hashTrimestres{$i} Trimestre $paramAnio | $totalesTrimestre{$centro}{$i}");
				print "\n---------------------------------------------------------\n";
				$totalTrimestre+=$totalesTrimestre{$centro}{$i};
				$totalAnual+=$totalesTrimestre{$centro}{$i};
			}
			&customPrint("TOTAL $hashTrimestres{$i} Trimestre      | $totalTrimestre");
			print "\n---------------------------------------------------------\n";
			print "\n";

		}
		&customPrint("TOTAL ANUAL  $paramAnio     | $totalAnual \n");
		close (SALIDA);
		&imprimirReporte($outputFile, scalar(keys %totalesTrimestre));
	}
	else
	{
		print "La opcion seleccionada no es la correcta, presiona una tecla para continuar...";
		$inputline = <STDIN>;
	}

}

sub ejecutado
{

	&clearScreen;
	print "Presupuesto Ejecutado\n";
	print "\n";
	print "Ingrese el anio presupuestario: ";
	$paramAnio = &readSTDIN;
	print "\n";

	#Print provincias 
	%provincias = &getProvincias;
	%provinciasReverse = &ReverseHash(\%provincias);
	@provinciasKey=keys(%provincias);
	for ($i=1;$i<=24;$i++)
	{
		print "$i. $provincias{$i}\n";
	}
	print "25. Todas las provincias\n";
	print "\nSeleccione la/s provincias que desea ver separadas por ',':";
	@paramProvincia = split(',',&readSTDIN);
	@paramProvincia = &trimArray(\@paramProvincia);

	#Cargo todos los archivos segun las provincias elegidas;
	%actividades = &getActCentros;
	%centros = &getCentros;
	if(hasItem("25",\@paramProvincia))
	{
		%ejecutados = &getEjecutados;
	}
	else
	{
		%ejecutados = &getEjecutadosByProvincia(\%provinciasReverse, \@paramProvincia);
	}

	&openSalida("Ejecutado", $paramAnio);
	print SALIDA "Fecha;Centro ID;Nombre de Centro;Cod Act;Actividad;Trimestre;Gasto;Provincia;Control\n";
	foreach my $ejecutadoKey (sort keys %ejecutados) {
		print "\n";
		print "Fecha    | Centro ID | Nombre de Centro | Cod Act | Actividad | Trimestre | Gasto | Provincia | Control";
		print "\n---------------------------------------------------------\n";
		$total = 0;
		@arrayEjecutado = @{$ejecutados{$ejecutadoKey}};
		foreach my $lineaEjecutado (@arrayEjecutado) {
			@itemEjecutado = split(';',$lineaEjecutado);
			@centrosByAct = @{$actividades{$itemEjecutado[7]}};
			$control = &hasCentroActivity;
			&customPrint("$itemEjecutado[1] | $itemEjecutado[2] | $centros{$itemEjecutado[2]} | $itemEjecutado[7] | $itemEjecutado[3] | $itemEjecutado[4] | $itemEjecutado[5] | $itemEjecutado[8] | $control");
			print "\n---------------------------------------------------------\n";
			$itemEjecutado[5] =~ s/,/./g;
			$total+=$itemEjecutado[5];
		}
		&customPrint("TOTAL PROVINCIA $ejecutadoKey $total\n");

	}
	close (SALIDA);

	&imprimirReporte($outputFile, scalar(keys %ejecutados));


}

sub controlEjecutado{
	&clearScreen;
	print "Control del Presupuesto Ejecutado\n";
	print "\n";
	print "Ingrese el anio presupuestario: ";
	$paramAnio = &readSTDIN;
	print "\n";
	print "1) Primer Trimestre\n";
	print "2) Segundo Trimestre\n";
	print "3) Tercer Trimestre\n";
	print "4) Cuarto Trimestre\n";
	print "\n";
	print "Ingrese los trimestre que desea visualizar separados por ',': ";
	@paramTrimestre = split(',',&readSTDIN);
	@paramTrimestre = &trimArray(\@paramTrimestre);

	print "\n";
	print "0.0.0.1 Unico centro\n";
	print "0.0.0.1, 0.0.0.1.2, 0.0.0.1-1 Multiples centro\n";
	print "0.0.0.1* Rango de Centro que comiencen con 0.0.0.1\n";
	print "* Todos los centros\n";
	print "\n";
	print "Ingrese los centros que desea considerando los ejemplos anteriores:";
	@paramCentros = split(',',&readSTDIN);
	@paramCentros = &trimArray(\@paramCentros);

	%centros = &getCentros;
	%totalesTrimestre = &getSancionados;
	%actividades = &getActCentros;
	%ejecutados = &getEjecutadosByCentro(\@paramCentros,\@paramTrimestre);
	%trimestres = &getTrimestres;
	%trimestresFecha = &getTrimestresInicio;
	$outputFile = "ControlEjecutado";
	&openSalida($outputFile, $paramAnio);
	print SALIDA "Fecha;Centro ID;Nombre de Centro;Cod Act;Actividad;Trimestre;Importe;Saldo por Trimestre;Saldo Acumulado;Control\n";
	foreach $centroKey (keys %ejecutados)
	{
		$totalCentro = 0;
		$saldoAcumulado = 0;
		print "\n";
		print "Fecha    | Centro ID | Nombre de Centro | Actividad | Trimestre | Importe | Saldo por Trimestre | Saldo Acumulado | Control";
		print "\n---------------------------------------------------------\n";
		foreach $trimestreKey (sort keys %{$ejecutados{$centroKey}})
		{
			$countTrimestre = 0;
			@arrayEjecutado = @{$ejecutados{$centroKey}{$trimestreKey}};
			foreach my $lineaEjecutado (@arrayEjecutado)
			{	
			 	@itemEjecutado = split(';',$lineaEjecutado);
				if($countTrimestre == 0)
				{
					$saldoAcumulado += $totalesTrimestre{$centroKey}{$trimestreKey};
					$saldoTrimestre = $totalesTrimestre{$centroKey}{$trimestreKey};
					$movimientoSancionado = $totalesTrimestre{$centroKey}{$trimestreKey};
					#TODO Agregar fecha trimestre
					&customPrint ("$trimestresFecha{$trimestreKey} | $centroKey | $centros{$centroKey} | 0 | $trimestres{$trimestreKey} | $movimientoSancionado | $movimientoSancionado | $saldoAcumulado | \n");
				}

				#EJECUTADOS
				$control = &hasCentroActivity;
				$itemEjecutado[5] =~ s/,/./g;
				$saldoAcumulado -= $itemEjecutado[5];
				$saldoTrimestre -= $itemEjecutado[5];
				$excedente = &gastoExcedido($saldoTrimestre);
				&customPrint("$itemEjecutado[1] | $centroKey | $centros{$centroKey} | $itemEjecutado[3] | $trimestres{$trimestreKey} | $itemEjecutado[5] | $saldoTrimestre | $saldoAcumulado | $excedente  $control\n");

				$countTrimestre += 1;
			}
			
		}
	}
	close (SALIDA);
	&imprimirReporte($outputFile, scalar(keys %ejecutados));


}


sub showMenu
{
	while (1) {
		&clearScreen;
		&cleanVars;
		print "\n";
		print "Bienvenido al sistema de Listep \n";
		print " \n";
		print "1) Presupuesto Sancionado\n";
		print "2) Presupuesto Ejecutado\n";
		print "3) Control del Presupuesto Ejecutado\n";
		print "4) Salir\n";
		print " \n";
		print "Seleccione una opcion: ";

		$inputMenu = &readSTDIN;

		eval {
			if ($inputMenu == "1")
			{
				&sancionado;
			}
			elsif($inputMenu == "2")
			{
				&ejecutado;
			}
			elsif($inputMenu == "3")
			{
				&controlEjecutado;
			}
			elsif ($inputMenu == "4")
			{
				exit;
			}
		};
		
	}


}

use utf8;
$dirmae = $ENV{"DIRMAE"};
$dirproc = $ENV{"DIRPROC"};
$dirinfo = $ENV{"DIRINFO"};
&showMenu;
