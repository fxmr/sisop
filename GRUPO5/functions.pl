#Subrutine
#@Description Eliminar el contenido de la consola
#@Params 
#@Return 
sub clearScreen{
	#system('clear');
}

sub cleanVars
{
	%totalesTrimestre = ();
	%ejecutados = ();
}

#Subrutine
#@Description Obtener la entrada standard eliminando el caracter de retorno
#@Params 
#@Return entrada estandard
sub readSTDIN{
	$inputline = <STDIN>;
	chomp($inputline);
	$inputline=~ s/^\s+|\s+$//g;
	$retval = $inputline;
}


#Subrutine
#@Description Imprime por pantalla y en un archivo
#@Params $outputLine con el texto a imprimir. Utiliza la variable SALIDA para escribir en el archivo
#@Return
sub customPrint{
	my ($outputLine) = @_;
	print $outputLine;
	@campos = split('\|',$outputLine);
	for (my $i = 0; $i <= $#campos; $i++) {
		$campos[$i] =~ s/^\s+|\s+$//g; 
	}
	$camposCSV = join(';',@campos); 
	print SALIDA $camposCSV."\n";
}

sub trimArray
{
	@array = @{$_[0]};
	for (my $i = 0; $i <= $#array; $i++) {
		$array[$i] =~ s/^\s+|\s+$//g; 
	}
	return @array;
}

#Subrutine
#@Description Obtener parametros del día actual
#@Params 
#@Return Hash => Key: Parametro (día, hora, mes), Value: Valor
sub today{
	($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime;
	$year+=1900;
	$mon++;
	%retval = ("sec", $sec, "min", $min, "hour", $hour, "dia", $mday, "mes", $mon, "anio", $year);
}

#Subrutine
#@Description Verifica si el usuario desea conservar el reporte
#@Params nombre del archivo a conservar
#@Return 
sub imprimirReporte
{
	my ($outputFile, $countData) = @_;
	if($countData > 0)
	{
		print "\nDesea imprimir el reporte (1)Si (2)No:";
		$inputline &readSTDIN;
		if($inputline == 1)
		{
			rename "${dirinfo}/tmp_$outputFile","${dirinfo}/$outputFile";
		}
	}
	else
	{
		print "No se ha encontrado resultados para el reporte solicitado, persione una tecla para continuar...";
		&readSTDIN;
	}
	if(-f "${dirinfo}/tmp_$outputFile")
	{
		unlink "${dirinfo}/tmp_$outputFile";
	}
}

#Subrutine
#@Description Abre en modo escritura el archivo de reporte
#@Params Nombre Reporte, Año de Reporte
#@Return Manejar en el archivo con SALIDA
sub openSalida
{
	my ($reporteName, $paramAnio) = @_;
	%today = &today;
	$now = "$today{anio}$today{mes}$today{dia}$today{hour}$today{min}$today{sec}";
	$outputFile = "Reporte_${reporteName}_${paramAnio}_${now}.csv";

	open (SALIDA,">", "${dirinfo}/tmp_$outputFile") || die "ERROR: No puedo abrir el fichero $entrada [$!]\n";
}

#Subrutine
#@Description Verificar si el array contiene el item
#@Params $item, $array con valores
#@Return 0 / 1
sub hasItem 
{
	my $item = $_[0];
	my @array = @{$_[1]};
	for (my $i = 0; $i <= $#array; $i++) {
		if ($array[$i] eq $item)
		{
			return 1;
		}
	}
	my $retval = 0;
}

#Subrutine
#@Description Invertir Hash
#@Params 
#@Return Hash invertido
sub ReverseHash
{
	my %hash = %{$_[0]};
	while (($key, $value) = each %hash) {
   		$hashReverse{$value}=$key;
   	}
   	%retval = %hashReverse
}

#Subrutine
#@Description Verifica si el centro tiene actividad planificada
#@Params Utiliza las variables $actividades e $itemEjecutado
#@Return Hash => string con el resultado
sub hasCentroActivity
{
	@centrosByAct = @{$actividades{$itemEjecutado[7]}};
	if(&hasItem($itemEjecutado[2],\@centrosByAct))
	{
		return "";
	}
	else
	{
		return ("Fuera de la planificacion");
	}
}

sub checkCentroFilter
{
	my ($centro) = $_[0];
	my (@arrayCentros) = @{$_[1]};
	for (my $i = 0; $i <= $#arrayCentros; $i++) {
		$starIndex = index($arrayCentros[$i],'*');
		@itemSinStar = split('\*',$arrayCentros[$i]);
		if ($starIndex == 0)
		{
			#Expansion de todos los centros
			return 1;
		}
		elsif ($starIndex > 0 & index($centro,$itemSinStar[0]) == 0)
		{
			#Si posee una expansion de centros verifica que el item coincida es el inicio
			return 1;
		}
		elsif ($starIndex == -1 & $arrayCentros[$i] eq $centro)
		{
			#Verifica que coincida exactamentes
			return 1;
		}
	}
	return 0;
}

#Subrutine
#@Description Verifica si el gasto trimestral está excedido
#@Params Recibe el gasto trimestral
#@Return Hash => string con el resultado
sub gastoExcedido
{
	my ($gastoTrimestral) = @_;
	if($gastoTrimestral < 0)
	{
		return "Presupuesto excedido";
	}
	else
	{
		return ("");
	}
}


#Subrutine
#@Description Obtener el hash Posicion / Trimestre
#@Params 
#@Return Hash => Key:Posicion, Value: nombre trimestre
sub getTrimestres
{
	%trimestres = ("0","Primer ","1","Segundo", "2","Tercer ","3","Cuarto ");
}

#Subrutine
#@Description Obtener el orden del trimestre segun el nombre 
#@Params $nombre trimestre
#@Return Posicion de trimestre
sub getTrimestrePosition
{
	my($trimestreName) = @_;
	if (index($trimestreName, "Primer") != -1)
	{
		$position = 0;
	}
	if (index($trimestreName, "Segundo") != -1)
	{
		$position = 1;
	}
	if (index($trimestreName, "Tercer") != -1)
	{
		$position = 2;
	}
	if (index($trimestreName, "Cuarto") != -1)
	{
		$position = 3;
	}
	$retval = $position;
}


#Subrutine
#@Description Obtener Provincias de archivo
#@Params 
#@Return Hash => Key: ProvinciaID, Value: Provincia Nombre
sub getProvincias
{
	$lineNum = 0;
	$fileName = "${dirmae}/provincias.csv";
	if(-f $fileName)
	{
		open (PROVINCIAS,"<", "$fileName") || die "ERROR: No puedo abrir el fichero $fileName [$!]\n";
		while ($linea=<PROVINCIAS>)
		{
			if($lineNum != 0)
			{
				chop($linea);
				@item = split(';',$linea);
				$provincias{$item[0]} = $item[1];
			}
			$lineNum++;
		}
		close (PROVINCIAS);
	}
	else
	{
		print "\nEl archivo $fileName no existe, presione una tecla para regresar...\n";
		&readSTDIN;
		die "File $fileName not found";
	}
	%retval = %provincias;
}


#Subrutine
#@Description Obtener informacion del archivo AxC
#@Params 
#@Return Hash => Key: ActID, Value: array CentroID
sub getActCentros
{
	my $lineNum = 0;
	$fileName = "${dirmae}/tabla-AxC.csv";
	if(-f $fileName)
	{
		open (AxC,"<", "$fileName") || die "ERROR: No puedo abrir el fichero $fileName [$!]\n";
		while ($linea=<AxC>)
		{
			if($lineNum != 0)
			{
				chop($linea);
				@itemAct = split(';',$linea);
				push (@{$actividades{$itemAct[0]}}, $itemAct[1]);
			}
			$lineNum++;
		}
		close (AxC);
	}
	else
	{
		print "\nEl archivo $fileName no existe, presione una tecla para regresar...\n";
		&readSTDIN;
		die "File $fileName not found";
	}
	return (%actividades);
}

#Subrutine
#@Description Obtener Fechas de inicio de trimestres
#@Params $paramAnio con el año seleccionado
#@Return Hash => Key: Trimestre Position, Value: Fecha inicio trimestre
sub getTrimestresInicio
{
	$lineNum = 0;
	$fileName = "${dirmae}/trimestres.csv";
	if(-f $fileName)
	{
		open (TRIMESTRES,"<", "$fileName") || die "ERROR: No puedo abrir el fichero $fileName [$!]\n";
		while ($linea=<TRIMESTRES>)
		{
			if($lineNum != 0)
			{
				chop($linea);
				@itemTrimestre = split(';',$linea);
				if(index($itemTrimestre[0],$paramAnio) >= 0)
				{
					$trimestrePosition = getTrimestrePosition($itemTrimestre[1]);
					@fechaTrimestre = split('/',$itemTrimestre[2]);
					$centros{$trimestrePosition} = "${fechaTrimestre[2]}${fechaTrimestre[1]}${fechaTrimestre[1]}";
				}
			}
			$lineNum++;
		}
		close (TRIMESTRES);
	}
	else
	{
		print "\nEl archivo $fileName no existe, presione una tecla para regresar...\n";
		&readSTDIN;
		die "File $fileName not found";
	}
	%retval = %centros;
}

#Subrutine
#@Description Obtener Centros de archivo
#@Params 
#@Return Hash => Key: CentroID, Value: Centro Nombre
sub getCentros
{
	$lineNum = 0;
	$fileName = "${dirmae}/centros.csv";
	if(-f $fileName)
	{
		open (CENTROS,"<", "$fileName") || die "ERROR: No puedo abrir el fichero $fileName [$!]\n";
		while ($linea=<CENTROS>)
		{
			if($lineNum != 0)
			{
				chop($linea);
				@itemCentro = split(';',$linea);
				$centros{$itemCentro[0]} = $itemCentro[1];
			}
			$lineNum++;
		}
		close (CENTROS);
	}
	else
	{
		print "\nEl archivo $fileName no existe, presione una tecla para regresar...\n";
		&readSTDIN;
		die "File $fileName not found";
	}
	%retval = %centros;
}

#Subrutine
#@Description Obtener Ejecutados ordenados en un hash por provincias
#@Params 
#@Return Hash => Key: Provincia, Value: array de ejecutados
sub getEjecutados
{
	$lineNum = 0;
	$fileName = "${dirproc}/ejecutado-${paramAnio}";
	if(-f $fileName)
	{
		open (EJECUTADOS,"<", "$fileName") || die "ERROR: No puedo abrir el fichero $fileName [$!]\n";
		while ($linea=<EJECUTADOS>)
		{
			if($lineNum != 0)
			{
				chop($linea);
				@itemEjecutado = split(';',$linea);
				$itemProvincia = $itemEjecutado[8];
				push (@{$ejecutados{$itemProvincia}}, $linea);
			}
			$lineNum++;
		}
		close (EJECUTADOS);
	}
	else
	{
		print "\nNo se encuentran datos para el anio $paramAnio, presione una tecla para regresar...\n";
		&readSTDIN;
		die "File $fileName not found";
	}
	%retval = %ejecutados;
}

#Subrutine
#@Description Obtener Ejecutados ordenados en un hash por provincias y filtrando por las provincias paramProvincia
#@Params Hash (ProvinciaNombre, ProvinciaKey), Array de provincias Seleccionadas
#@Return Hash => Key: Provincia, Value: array de ejecutados
sub getEjecutadosByProvincia
{
	$lineNum = 0;
	my %provinciasReverse = %{$_[0]};
	my @paramProvincia = @{$_[1]};
	$fileName = "${dirproc}/ejecutado-${paramAnio}";
	if(-f $fileName)
	{
		open (EJECUTADOS,"<", "$fileName") || die "ERROR: No puedo abrir el $fileName $entrada [$!]\n";
		while ($linea=<EJECUTADOS>)
		{
			if($lineNum != 0)
			{
				chop($linea);
				@itemEjecutado = split(';',$linea);
				$itemProvincia = $itemEjecutado[8];
				if(&hasItem($provinciasReverse{$itemProvincia}, \@paramProvincia))
				{
					push (@{$ejecutados{$itemProvincia}}, $linea);
				}
			}
			$lineNum++;
		}
		close (EJECUTADOS);
	}
	else
	{
		print "\nNo se encuentran datos para el anio $paramAnio, presione una tecla para regresar...\n";
		&readSTDIN;
		die "File $fileName not found";
	}
	%retval = %ejecutados;
}

#Subrutine
#@Description Obtener Ejecutados ordenados en un hash por centros y trimestres
#@Params 
#@Return Hash => Key: Centro, Value: Hash => Key: Trimestre, Value: array de actividades
sub getEjecutadosByCentro
{
	$lineNum = 0;
	my @paramCentros = @{$_[0]};
	my @paramTrimestre = @{$_[1]};
	$fileName = "${dirproc}/ejecutado-${paramAnio}";
	if(-f $fileName)
	{
		open (EJECUTADOS,"<", "$fileName") || die "ERROR: No puedo abrir el $fileName [$!]\n";
		while ($linea=<EJECUTADOS>)
		{
			if($lineNum != 0)
			{
				chop($linea);
				@itemEjecutado = split(';',$linea);
				$itemTrimestre = $itemEjecutado[4];
				$itemCentro = $itemEjecutado[2];
				$trimestrePosition = &getTrimestrePosition ($itemTrimestre);
				if((&hasItem($trimestrePosition + 1,\@paramTrimestre)) & &checkCentroFilter($itemCentro,\@paramCentros))
				{
					push (@{$ejecutados{$itemCentro}{$trimestrePosition}}, $linea);
				}
			}
			$lineNum++;
		}
		close (EJECUTADOS);
	}
	else
	{
		print "\nNo se encuentran datos para el anio $paramAnio, presione una tecla para regresar...\n";
		&readSTDIN;
		die "File $fileName not found";
	}
	%retval = %ejecutados;
}

#Subrutine
#@Description Obtener Sancionados de archivo
#@Params 
#@Return Hash => Key: CentroID, Value: Hash => Key Trimestre Position, Value: Nombre Trimestre
sub getSancionados
{
	$fileName = "${dirmae}/sancionado-${paramAnio}.csv";
	if(-f $fileName)
	{
		open (SANCIONADOS, "<", "${fileName}") || die "ERROR: No puedo abrir el fichero $fileName [$!]\n";
		$lineNum = 0;
		while ($linea=<SANCIONADOS>)
		{
			if($lineNum != 0)
			{
				chop($linea);
				@itemSancionado = split(';',$linea);
				$centroID = $itemSancionado[0];
				$itemSancionado[3] =~ s/,/./g;
				$itemSancionado[4] =~ s/,/./g;
				$total = $itemSancionado[2]+$itemSancionado[3];
				$trimestrePosition = &getTrimestrePosition ($itemSancionado[1]);
				$totalesTrimestre{$centroID}{$trimestrePosition} += $total;

			}
			$lineNum++;
		} 
		close (SANCIONADOS);
	}
	else
	{
		print "\nEl archivo $fileName no existe, presione una tecla para regresar...\n";
		&readSTDIN;
		die "File $fileName not found";
	}
	%retval = %totalesTrimestre;
}

return 1;