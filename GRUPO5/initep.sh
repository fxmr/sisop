#!/bin/bash

Initep="Initep"

function log {
	echo -e "$3 $2"
	if [[ -f logep.sh ]]; then
		if [[ ! -x logep.sh ]]; then
			chmod +x logep.sh
		fi
		./logep.sh "$1" "$2" "$3" # 1=nombre del script que loguea, 2=log message, 3=log level
	fi
}

function asignarPermisos {
	log $Initep "Chequeando permisos de archivos" "INFO"

	if ! [[ -x "$DIRBIN/deamonep.sh" ]]; then
		echo "Modificando los permisos de deamonep.sh"
		chmod +x "DIRBIN/deamonep.sh"
	fi
	if ! [[ -x "$DIRBIN/procep.sh" ]]; then
		echo "Modificando los permisos de procep.sh"
		chmod +x "./procep.sh"
	fi
	if ! [[ -x "$DIRBIN/listep.sh" ]]; then
		echo "Modificando los permisos de listep.sh"
		chmod +x "./listep.sh"
	fi
	if ! [[ -x "$DIRBIN/movep.sh" ]]; then
		echo "Modificando los permisos de movep.sh"
		chmod +x "./movep.sh"
	fi
	if ! [[ -r "$DIRMAE/centros.csv" ]]; then
		echo "Modificando los permisos de centros.csv"
		chmod +r "$DIRMAE/centros.csv"
	fi
	if ! [[ -r "$DIRMAE/provincias.csv" ]]; then
		echo "Modificando los permisos de proviencias.csv"
		chmod +r "$DIRMAE/provincias.csv"
	fi
	if ! [[ -r "$DIRMAE/trimestres.csv" ]]; then
		echo "Modificando los permisos de trimestres.csv"
		chmod +r "$DIRMAE/trimestres.csv"
	fi
	if ! [[ -w "$DIRLOG/Initep.log" ]]; then
		echo "Modificando los permisos de Initep.log"
		chmod +w "$DIRLOG/Initep.log"
	fi
	#if ! [[ -w "$DIRLOG/deamonep.log" ]]; then
	#	echo "Modificando los permisos de deamonep.log"
	#	chmod +w "$DIRLOG/deamonep.log"
	#fi
}

function archivoExistente {
	local aux
	aux=$(ls -1 "$1" | grep "^$2$")
	if [[ "$aux" == "$2" ]]; then
		return 0
	fi
	return 1
}

function verificarArchivos {
	local archivosFaltantes=""

	archivoExistente "." "deamonep.sh"
	if [[ "$?" -eq 1 ]]; then
		archivosFaltantes+="./deamonep.sh "
	fi
	archivoExistente "." "procep.sh"
	if [[ "$?" -eq 1 ]]; then
		archivosFaltantes+="./procep.sh "
	fi
	archivoExistente "." "listep.sh"
	if [[ "$?" -eq 1 ]]; then
		archivosFaltantes+="./listep.sh "
	fi
	archivoExistente "." "logep.sh"
	if [[ "$?" -eq 1 ]]; then
		archivosFaltantes+="./logep.sh "
	fi
	archivoExistente "." "movep.sh"
	if [[ "$?" -eq 1 ]]; then
		archivosFaltantes+="./movep.sh "
	fi
	archivoExistente "$DIRMAE" "centros.csv"
	if [[ "$?" -eq 1 ]]; then
		archivosFaltantes+="$DIRMAE/centros.csv "
	fi
	archivoExistente "$DIRMAE" "provincias.csv"
	if [[ "$?" -eq 1 ]]; then
		archivosFaltantes+="$DIRMAE/provincias.csv "
	fi
	archivoExistente "$DIRMAE" "trimestres.csv"
	if [[ "$?" -eq 1 ]]; then
		archivosFaltantes+="$DIRMAE/trimestres.csv "
	fi
	echo "$archivosFaltantes"
}

function verificarDirectorios {
	local directoriosFaltantes=""

	if [[ ! -d "$DIRBIN"  || "$DIRBIN" == "" ]]; then
		directoriosFaltantes+="directorio de archivos binarios $DIRBIN "
	fi

	if [[ ! -d "$DIRMAE" || "$DIRMAE" == "" ]]; then
		directoriosFaltantes+="directorio de archivos maestro y tablas $DIRMAE "
	fi

	if [[ ! -d "$DIRREC" || "$DIRREC" == "" ]]; then
		directoriosFaltantes+="directorio de archivos novedades $DIRREC "
	fi

	if [[ ! -d "$DIROK" || "$DIROK" == "" ]]; then
		directoriosFaltantes+="directorio de archivos aceptados $DIROK "
	fi

	if [[ ! -d "$DIRPROC" || "$DIRPROC" == "" ]]; then
		directoriosFaltantes+="directorio de archivos procesados $DIRPROC "
	fi

	if [[ ! -d "$DIRINFO" || "$DIRINFO" == "" ]]; then
		directoriosFaltantes+="directorio de archivos reportes $DIRINFO "
	fi

	if [[ ! -d "$DIRLOG" || "$DIRLOG" == "" ]]; then
		directoriosFaltantes+="directorio de archivos log $DIRLOG "
	fi

	if [[ ! -d "$DIRNOK" || "$DIROK" == "" ]]; then
		directoriosFaltantes+="directorio de archivos rechazados $DIRNOK "
	fi

	echo "$directoriosFaltantes"
}


# verifica el archivo e configuracion
PATH_ARCHIVO_DE_CONFIGURACION="../dirconf/Instalep.conf" #TODO harcodeo

if ! [ -f $PATH_ARCHIVO_DE_CONFIGURACION ]; then    # -f: si existe el archivo
	echo "ERROR: No se encontro el archivo de configuracion. Por favor, correr nuevamente Instalep.sh"
	export $ambienteSeteado=false
	exit 1
fi

# setea las variables de ambiente
if [[ "$ambienteSeteado" == "" ]]; then

	#GRUPO=/usr/alumnos/temp/grupo01=alumnos=09/04/2015 10:03 p.m
	#solo me tengo que quedar con /usr/alumnos/temp/grupo01 | elimino Grupo= | elimino lo despues del =

	## FIARME COMO VEO SI NO PUEDO INICIALIZAR LAS VARIABLES,
	## POR EJEMPL QUE NO ESTE DENTRO DE CONFIG TENGO QUE MANDAR ERROR Y TERMINAR

	GRUPO=$(grep 'GRUPO=' "$PATH_ARCHIVO_DE_CONFIGURACION" | sed "s/GRUPO=//" | sed "s/=.*//")
	export GRUPO

	BIN=$(grep 'DIRBIN=' "$PATH_ARCHIVO_DE_CONFIGURACION" | sed "s/DIRBIN=//" | sed "s/=.*//")
	DIRBIN=$GRUPO/$BIN/
	export DIRBIN

	LOG=$(grep 'DIRLOG=' "$PATH_ARCHIVO_DE_CONFIGURACION" | sed "s/DIRLOG=//" | sed "s/=.*//")
	DIRLOG=$GRUPO/$LOG/
	export DIRLOG
	chmod +xrw "./logep.sh"

	log $Initep "Creando variables de entorno" "INFO"

	MAE=$(grep 'DIRMAE=' "$PATH_ARCHIVO_DE_CONFIGURACION" | sed "s/DIRMAE=//" | sed "s/=.*//")
	DIRMAE=$GRUPO/$MAE/
	export DIRMAE

	REC=$(grep 'DIRREC=' "$PATH_ARCHIVO_DE_CONFIGURACION" | sed "s/DIRREC=//" | sed "s/=.*//")
	export DIRREC=$GRUPO/$REC/

	OK=$(grep 'DIROK=' "$PATH_ARCHIVO_DE_CONFIGURACION" | sed "s/DIROK=//" | sed "s/=.*//")
	export DIROK=$GRUPO/$OK/

	INFO=$(grep 'DIRINFO=' "$PATH_ARCHIVO_DE_CONFIGURACION" | sed "s/DIRINFO=//" | sed "s/=.*//")
	export DIRINFO=$GRUPO/$INFO/

	NOK=$(grep 'DIRNOK=' "$PATH_ARCHIVO_DE_CONFIGURACION" | sed "s/DIRNOK=//" | sed "s/=.*//")
	export DIRNOK=$GRUPO/$NOK/
	
	DIRPROC=$(grep 'DIRPROC=' "$PATH_ARCHIVO_DE_CONFIGURACION" | sed "s/DIRPROC=//" | sed "s/=.*//")
	export DIRPROC=$GRUPO/$DIRPROC/


	# verifica faltantes de directorios y archivos
	log $Initep "Verificando instalacion" "INFO"
	dir_falt=$(verificarDirectorios)
	arch_falt=$(verificarArchivos)

	if [[ -e "$dir_falt"  ]] || [[ -e "$arch_falt" ]]; then
		echo "$dir_falt$arch_falt"
		log $Initep "Se detectaron faltantes o errores en la instalacion del sistema. Por favor, correr initep.sh antes de continuar" "ERROR"
		if [[ -e "$arch_falt" ]]; then
			log $Initep "Directorios faltantes: $dir_falt" "WARNING"
		fi
		if [[ -e "$arch_falt" ]]; then
			log $Initep "Archivos faltantes: $arch_falt" "WARNING"
		fi
		log $Initep "No se pudo iniciar el sistema" "INFO"
		#finalizar=true
		exit
	fi

 	asignarPermisos
 	log $Initep "Estado del sistema: INICIALIZADO" "INFO"
 	ambienteSeteado=true
	export ambienteSeteado

		deamonActivar=""
 	while [[ "$deamonActivar" != "Si" && "$deamonActivar" != "No" ]]
 	do
		echo "¿Desea efectuar la activación de deamonep? Si – No"
		read -r deamonActivar
		#deamonActivar="No"
	done
	log $Initep "¿Desea efectuar la activación de deamonep? Si – No: $deamonActivar" "INFO"

	if [[ "$deamonActivar" == "Si" ]];then
		$DIRBIN/start_deamonep.sh&
		sleep 1s;
		pid=`pgrep 'deamonep.sh'`
		log $Initep "deamonep corriendo bajo el no.:${pid}" "INFO"

	elif [[ "$deamonActivar" == "No" ]];then
		# EXPLICAR LA FORMA MANUAL
		echo 'Para iniciar deamonep llamar a ./start_deamonep.sh'
	fi



else
#elif [[ $ambienteSeteado==true ]]; then
	# No logueo porque no existen las variables de ambiente
	echo "Ambiente ya  inicializado, para reiniciar termine la sesión e ingrese nuevamente. "
	#finalizar=true
	#exit
fi


##$SHELL
