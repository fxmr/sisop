#!/bin/bash

Movep=$BINDIR"./movep.sh"
Logep=$BINDIR"./logep.sh"

#archivos_ok=`ls $DIROK`

#METER LOCAL A LAS VARIABLES???


function cantidad_archivos {
	#Cuento cantidad de archivos a procesar
	#ver si funciona con la direccion asi
	cantidad=$( ls -l $DIROK/*.csv | wc -l)

	#echo "Cantidad: $cantidad"

	$Logep "procep" "INFO" "Cantidad de archivos a procesar: $cantidad"
}

function archivo_duplicado {
	local archivo="$DIRPROC/proc/$1"
	if [ -f $archivo ]
		then
			$Logep "procep" "INFO" "Archivo Duplicado. Se rechaza el archivo $1"
			return 1
		else
			return 0
	fi
}

function validacion_cantidad_campos {
	read -r FIRSTLINE < $1
	campos=$( echo $FIRSTLINE | grep -o ";" | wc -l)
	# 5 porque cuenta la cantidad de ;
	if [ $campos -ne 5 ]
		then 
			#echo "Campos: $campos"
			$Logep "procep" "INFO" "Estructura inesperada. Se rechaza el archivo $1"
			return 1
		else
			return 0
	fi
}

function validacion_centro {
	centro=$( echo "$1"| cut -d';' -f3 )
	centro_en_maestro=$( grep "^$centro;[^;]*$" $DIRMAE/centros.csv )
	if [ $? -ne 0 ]
		then 
			#Centro no encontrado en maestro
			msj="Centro inexistente"
			return 1
		else
			nombre_centro="$( echo $centro_en_maestro| cut -d';' -f2 )"
			return 0
	fi
}

function validacion_actividad {
	actividad="$( echo "$1"| cut -d';' -f4 )"

	actividad_en_maestro="$( grep "^[^;]*;[^;]*;[^;]*;$actividad$" $DIRMAE/actividades.csv )"
	if [ $? -ne 0 ]
		then 
			#Actividad no encontrada en maestro
			msj="Actividad inexistente"
			return 1
		else
			cod_actividad="$( echo "$actividad_en_maestro"| cut -d';' -f1 )"
			return 0
	fi
}

#CORTAR TRIMESTRE DEL REGISTRO EN MAIN Y PASAR
# trimestre = "$( echo $1| cut -d';' -f5 )"
#Parametro 1 es el trimestre y el 2 es el nombre del archivo o el año (q se corta en el main) ELEGIR
function validacion_trimestre {
	local error=$(( 0 ))
	#Pongo comillas para que no me separe el campo por espacios

	trimestre_en_fc="$1"

	#echo "trimestre en fc: $trimestre_en_fc"
	
	trimestre_en_tabla="$( grep "$1" $DIRMAE/trimestres.csv )"

	#echo "trimestre en tabla : $trimestre_en_tabla"

	if [ $? -ne 0 ]
		then 
			#Trimestre no encontrado en tabla
			local error=$(( 1 ))
		else
			#anio_trimestre=$( echo $1| cut -c18-)
			#echo "Anio pasado por parametro: $2"

			#echo "Trimestre en funcion: $1"

			anio_trimestre=$( echo "$1"| cut -d ' ' -f3 )

			#echo "Anio trimestre: $anio_trimestre"

			if [ $2 -ne $anio_trimestre ]
			then
				local error=$(( 1 ))
			fi
	fi

	if [ $error -eq 1 ]
		then 
			msj="Trimestre invalido"
			return 1
		else
			return 0
	fi	
}

#CORTAR FECHA DEL REGISTRO EN MAIN Y PASAR POR PARAM
#yyyymmdd = $( echo $registro| cut -d';' -f2 )
function validacion_fecha_valida {
	
	anio=$( echo "$1"| cut -c-4 )
	mes=$( echo "$1"| cut -c5-6 )
	dia=$( echo "$1"| cut -c7- )

	local mesdiaanio="${1:4:2}/${1:6:2}/${1:0:4}"

	#echo "Fecha $mesdiaanio"

	[[ $(date -d "${mesdiaanio//-/\/}" 2> /dev/null) ]] && ret=1 || ret=0
	# mmddyyy = "$mes-$dia-$anio"
	#aniomesdia="$anio-$mes-$dia"
	#date -d "$aniomesdia" 2>&1 +/dev/null
	#if [ $? -ne 0 ]
	if [ $ret -eq 0 ]
		then 
			#echo "Fecha invalida"
			msj="Fecha invalida"
			return 1
		else
			return 0
	fi
}

#parametro 1 trimestre del registro
#parametro 2 fecha del registro cortada en main
#parametro 3 nombre del archivo 
function validacion_fecha_correspondiente {
	fecha_archivo=$( echo $3| cut -d'_' -f4 | cut -c-8)

	if [ $2 -le $fecha_archivo ] 
		then 
			trimestre_en_tabla="$( grep "$1" $DIRMAE/trimestres.csv )" 
			f_desde_tri_datos=$( echo $trimestre_en_tabla| cut -d';' -f3 )
			f_hasta_tri_datos=$( echo $trimestre_en_tabla| cut -d';' -f4 )

			#Transformo a formato yyyymmdd
			anio_desde=$( echo $f_desde_tri_datos| cut -c7- )
			mes_desde=$( echo $f_desde_tri_datos| cut -c4-5 )
			dia_desde=$( echo $f_desde_tri_datos| cut -c-2 )
			f_desde_tri="$anio_desde$mes_desde$dia_desde"

			anio_hasta=$( echo $f_hasta_tri_datos| cut -c7- )
			mes_hasta=$( echo $f_hasta_tri_datos| cut -c4-5 )
			dia_hasta=$( echo $f_hasta_tri_datos| cut -c-2 )
			f_hasta_tri="$anio_hasta$mes_hasta$dia_hasta"

			if [ $2 -ge $f_desde_tri ] && [ $2 -le $f_hasta_tri ]
				then 
					return 0
				else 
					msj="La fecha no se corresponde con el trimestre indicado"
					return 1
			fi
		else
			msj="La fecha no se corresponde con el año indicado"
			return 1
	fi
}


#Recibe por parametro el registro del arch
function validacion_gasto {
	#gasto=$( echo $1| cut -d';' -f1 | rev)
	gasto=$( echo $1| cut -d';' -f6 )
	#echo "Gasto ee: $gasto"

	#local num=$(( $gasto * 10 ))
	local num=$( echo $gasto | cut -d',' -f1 )

	if [ $num -le 0 ]
		then
			msj="Importe invalido"
			return 1
		else
			return 0
	fi
}

function rechazar_registro {
	(( reg_rechazados++ ))
	fecha=$( date +"%Y/%m/%d-%T")

	reporte="$archivo;$msj;"$registro";$USER;$fecha"

	echo $reporte >> $DIRPROC/rechazado-$anio_presupuestario
}


#MAIN 

#Verifico si el ambiente esta inicializado
if [ -z "$DIRBIN" ]
then
	echo "Error: el ambiente no fue inicializado."
	exit 1
fi

archivos=`ls $DIRREC`
rechazados="$DIRNOK"
#No deberia alcanzar solo con dirok?
#aceptados=`ls $DIROK/*.csv`
aceptados=`ls $DIROK`

if [ -n "$aceptados" ]
	then 
		cantidad_archivos 

		#echo "Dir proc: $DIRPROC"

		for archivo in $aceptados
		do
			#nombre_archivo="$DIROK/$archivo"

			#nombre_archivo=$( echo ${archivo##*/} )

			#echo "Archivo: $archivo"

			#archivo_a_recorrer=`tail -n +2 $DIROK/$archivo`

			anio_presupuestario=$( echo $archivo| cut -d'_' -f2 )

			archivo_duplicado $archivo

			if [ $? -eq 1 ]
				then 
					$Movep "$DIROK/$archivo" "$rechazados" "procep"
					continue
			fi

			validacion_cantidad_campos $DIROK/$archivo

			if [ $? -eq 1 ]
				then
					#echo "Entro a rechazar can campos" 
					$Movep "$DIROK/$archivo" "$rechazados" "procep"
					continue
			fi

			$Logep "procep" "INFO" "Archivo a procesar: $archivo"

			reg_totales=$(( 0 ))
			reg_aceptados=$(( 0 ))
			reg_rechazados=$(( 0 ))

			while read registro || [[ -n "$registro" ]]
			do
				(( reg_totales++ ))

				validacion_centro "$registro"

				if [ $? -eq 1 ]
					then 
						rechazar_registro
						continue
				fi

				validacion_actividad "$registro"

				if [ $? -eq 1 ]
					then 
						rechazar_registro
						continue
				fi

				trimestre="$( echo "$registro"| cut -d';' -f5 )"

				validacion_trimestre "$trimestre" $anio_presupuestario

				if [ $? -eq 1 ]
					then 
						rechazar_registro
						continue
				fi

				fecha_reg=$( echo "$registro"| cut -d';' -f2 )

				validacion_fecha_valida $fecha_reg

				if [ $? -eq 1 ]
					then 
						rechazar_registro
						continue
				fi

				validacion_fecha_correspondiente "$trimestre" $fecha_reg $archivo

				if [ $? -eq 1 ]
					then 
						rechazar_registro
						continue
				fi

				validacion_gasto "$registro"

				if [ $? -eq 1 ]
					then 
						rechazar_registro
						continue
				fi

				(( reg_aceptados++ ))

				#echo "reg aceptados: $reg_aceptados"

				id_reg=$( echo "$registro"| cut -d';' -f1 )
				cod_prov_arch=$( echo $archivo| cut -d'_' -f3)
				prov_en_maestro=$( grep "^$cod_prov_arch;[^;]*;[^;]*$" $DIRMAE/provincias.csv )
				nombre_prov="$( echo "$prov_en_maestro"| cut -d';' -f2 )"

				reporte_acep="$id_reg;$fecha_reg;$centro;"$actividad";"$trimestre";$gasto;$archivo;$cod_actividad;$nombre_prov;$nombre_centro"
				
				echo $reporte_acep >> $DIRPROC/ejecutado-$anio_presupuestario

			done <<< "$( tail -n +2 $DIROK/$archivo )" 

			#$Movep "$archivo" "$DIRPROC/proc/$nombre_archivo" "procep"
			$Movep "$DIROK/$archivo" "$DIRPROC/proc/" "procep"
			$Logep "procep" "INFO" "Registros totales: $reg_totales. Registros rechazados: $reg_rechazados. Registros validados: $reg_aceptados"

		done	
fi

			
